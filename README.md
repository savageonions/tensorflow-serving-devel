# TensorFlow Serving

This repository builds a Docker image that contains the TensorFlow Serving model server.
Once built, an exported model can be mounted inside the container and served to make predictions.

A prebuilt image is hosted on Docker Cloud but if a new image needs to be built, see below.
can be used.

## Requirements

- [Docker](https://docs.docker.com/engine/installation/)

## Usage

### Prebuilt image in Docker Cloud

    $ docker run --rm -v /path/to/model:/tmp/models -p 9000:9000 insectatorious/tensorflow-serving-devel --port=9000 --model_name="<model>" --model_base_path="/tmp/models"

### Building locally

    $ docker build -t <image_tag:version> .
    $ docker run --rm -v /path/to/model:/tmp/models -p 9000:9000 <image_tag:version> --port=9000 --model_name="<model>" --model_base_path="/tmp/models"

